var backlight = $('.segment > div');
var dial = $('.dial');
var solution = $('#solution');

function animationHandler() {
  backlight.removeClass('backlight').width();
  dial.removeClass('turn').width();
  backlight.addClass('backlight');
  dial.addClass('turn');
}

function animationStop() {
  backlight.removeClass('backlight').width();
  dial.removeClass('turn').width();
}

function reset() {
  animationHandler();
  container.empty();
  solution.empty();
}

function revealHandler(answer) {
  reveal.click(function () {
    solution.empty();
    animationStop();
    answer.split('').forEach(function (letter) {return (
        solution.append("<li>" + letter + "</li>"));});
  });
}

var wordArray = [
["QUITESOUR", "TURQUOISE"],
["FREEGROIN", "FOREIGNER"],
["SCARYPOLE", "COSPLAYER"],
["ADMITLANE", "LAMINATED"],
["DUNECRANE", "ENDURANCE"],
["QUITESEXI", "EXQUISITE"],
["TIGERNAME", "GERMINATE"],
["DIRTYROOM", "DORMITORY"],
["INFERCORE", "REINFORCE"],
["VEGDANCES", "SCAVENGED"],
["NOSEMIGHT", "SOMETHING"],
["NOSESTAIN", "SENSATION"],
["LOONYURGE", "NEUROLOGY"],
["DOGGIRUNN", "GROUNDING"],
["BEARDRENT", "BARTENDER"],
["GNATBITER", "BATTERING"],
["TOTEMSNOB", "TOMBSTONE"],
["WATCHLEER", "CARTWHEEL"],
["BLAZENOSY", "LAZYBONES"],
["EACHSTORK", "SHORTCAKE"]];

var container = $('#question');
var newGame = $('#new');
var reveal = $('#reveal');

function startGame() {
  reset();
  var question = wordArray[Math.floor(Math.random() * wordArray.length)];
  var scrambledArray = question[0].split('');
  scrambledArray.forEach(function (letter) {return (
      container.append("<li>" + letter + "</li>"));});
  var answer = question[1];
  container.sortable({
    axis: "x",
    stop: function stop() {
      var attempt = "";
      container.children().each(function () {
        attempt += $(this).text();
      });
      if (attempt === answer) {
        solution.empty();
        answer.split('').forEach(function (letter) {return (
            solution.append("<li>" + letter + "</li>"));});
        animationStop();
        window.alert("You won!");
      }
    } });

  revealHandler(answer);
}

newGame.click(startGame);